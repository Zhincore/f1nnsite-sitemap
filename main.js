import fs from "fs";
import fetch from "node-fetch";
import express from "express";
import {config} from "dotenv";
config();

export async function fetchAPI(path) {
  const requestUrl = `${process.env.NEXT_PUBLIC_STRAPI_API_URL}${path}`;
  const response = await fetch(requestUrl);
  if (response.status != 200) return null;
  try {
    return await response.json();
  } catch (err) { /**/ }
}

const cooldown = 30 * 60 * 1000; // 30 minute
const sitemapFiles  = process.cwd()+"/public/sitemap.";
const sitemapIndex  = sitemapFiles+"xml";
const sitemapMain   = sitemapFiles+"main.xml";
const sitemapOutfit  = sitemapFiles+"outfit.";

let lastFetch = 0;


const app = express();

app.get("/generate", (_, res) => {
  if (Date.now() - lastFetch > cooldown) {
    updateSitemap();
    lastFetch = Date.now();
  }
  res.sendStatus(202);
});

app.listen(process.env.PORT, () => {
  console.log("Listening on port "+process.env.PORT);
});

updateSitemap();

async function updateSitemap() {
  const pages = [
    { loc: "/faq" }, // Doesn't have lastmod
  ];

  const addLoc = (loc, lastmod) => pages.push({ loc, lastmod });

  const outfitPromises = [];
  await Promise.all([
    fetchAPI("/homepage").then(result => addLoc("/", result.updated_at)),
    fetchAPI("/about").then(result => addLoc("/about", result.updated_at)),
    fetchAPI("/outfits").then(outfits => {
      let latestUpdate;

      for (const outfit of outfits) {
        const updated = new Date(outfit.updated_at);
        if (!latestUpdate || latestUpdate < updated) latestUpdate = updated;

        outfitPromises.push(fetchMediaMap(outfit.slug));
      }

      addLoc("/outfits", latestUpdate && latestUpdate.toISOString());
    }),
    fetchAPI("/fanarts").then(fanarts => {
      let latestUpdate;

      for (const fanart of fanarts) {
        const updated = new Date(fanart.updated_at);
        if (!latestUpdate || latestUpdate < updated) latestUpdate = updated;

        // TODO: Map fanarts
      }

      addLoc("/fanarts", latestUpdate && latestUpdate.toISOString());
    }),
  ]);
  const outfits = await Promise.all(outfitPromises);

  //// SITEMAP INDEX
  fs.writeFileSync(sitemapIndex, [
    `<?xml version="1.0" encoding="UTF-8"?>`,
    `<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`,
      `<sitemap>`,
        `<loc>${encodeURI(`https://${process.env.NEXT_PUBLIC_DOMAIN}/sitemap.main.xml`)}</loc>`,
      `</sitemap>`,
      ...outfits.flatMap(([slug]) => [
        `<sitemap>`,
          `<loc>${encodeURI(`https://${process.env.NEXT_PUBLIC_DOMAIN}/sitemap.outfit.${slug}.xml`)}</loc>`,
        `</sitemap>`,
      ]),
    `</sitemapindex>`,
  ].join(""));

  //// MAIN SITEMAP
  fs.writeFileSync(sitemapMain, [
    `<?xml version="1.0" encoding="UTF-8"?>`,
    `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`,
      ...pages.flatMap(page => [
        `<url>`,
          `<loc>${encodeURI(`https://${process.env.NEXT_PUBLIC_DOMAIN+page.loc}`)}</loc>`,
          ...(page.lastmod ? [`<lastmod>${page.lastmod}</lastmod>`] : []),
        `</url>`,
      ]),
    `</urlset>`,
  ].join(""));

  //// OUTFIT SITEMAPS
  for (const [slug, posts, lastmod] of outfits) {
    fs.writeFileSync(sitemapOutfit+slug+".xml", [
      `<?xml version="1.0" encoding="UTF-8"?>`,
      `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">`,
        `<url>`,
          `<loc>${encodeURI(`https://${process.env.NEXT_PUBLIC_DOMAIN+"/outfits/"+slug}`)}</loc>`,
          ...(lastmod ? [`<lastmod>${lastmod}</lastmod>`] : []),
          ...posts.flatMap(({type, ...post}) => [
            `<${type}:${type}>`,
              `<${type}:title>`,
                post.title,
              `</${type}:title>`,
              `<${type}:${type === "image" ? "caption" : "description"}>`,
                post.caption,
              `</${type}:${type === "image" ? "caption" : "description"}>`,
              `<${type}:${type === "image" ? "loc" : "content_loc"}>`,
                encodeURI(`${process.env.NEXT_PUBLIC_STRAPI_API_URL}${post.loc}`),
              `</${type}:${type === "image" ? "loc" : "content_loc"}>`,
            `</${type}:${type}>`,
          ]),
        `</url>`,
      `</urlset>`,
    ].join(""));
  }

}

async function fetchMediaMap(slug) {
  return fetchAPI("/outfits/"+slug).then(outfit => {
    const posts = [];

    const addPost = media => posts.push({
      type: media.mime.split("/")[0],
      loc: media.url,
      title: media.alternativeText || `F1nn's ${outfit.name}`,
      caption: media.caption || `F1NN5TER in his ${outfit.name} outfit`,
    });

    if (outfit.icon) addPost(outfit.icon);
    if (outfit.posts) outfit.posts.forEach(addPost);
    return [slug, posts, outfit.updated_at];
  });
}
